/*
 * Copyright © 2018 AgBrain
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Author: Daniel Stone <daniel@fooishbar.org>
 */

#ifndef UTIL_REGION_H
#define UTIL_REGION_H

#include <stdio.h>

#include "util/u_box.h"
#include "util/u_vector.h"
#include "pipe/p_state.h"

struct pipe_region {
   struct pipe_box extent;
   struct u_vector boxes;
};

static inline void pipe_region_init(struct pipe_region *region)
{
   memset(&region->extent, 0, sizeof(region->extent));
   u_vector_init(&region->boxes, 32, 128); /* FIXME u_vector API is hostile */
}

static inline void pipe_region_fini(struct pipe_region *region)
{
   u_vector_finish(&region->boxes);
   memset(&region->extent, 0, sizeof(region->extent));
}

static inline bool pipe_region_overlaps(struct pipe_region *region,
                                        const struct pipe_box *in)
{
   struct pipe_box *cur;

   if (!u_box_test_intersection_2d(&region->extent, in))
      return false;

   u_vector_foreach(cur, &region->boxes) {
      if (u_box_test_intersection_2d(cur, in)) {
         fprintf(stderr, "\t\tINTERSECT: cur [%d, %d] + [%d, %d] intersects box [%d, %d] + [%d, %d]\n", cur->x, cur->y, cur->width, cur->height, in->x, in->y, in->width, in->height);
         return true;
      }
   }

   return false;
}

static inline bool pipe_region_is_empty(struct pipe_region *region)
{
   return region->extent.width == 0;
}

void pipe_region_add(struct pipe_region *region, const struct pipe_box *in);

#endif
