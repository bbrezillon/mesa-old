/*
 * Copyright © 2018 AgBrain
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Author: Daniel Stone <daniel@fooishbar.org>
 */

#include <stdio.h>

#include "util/u_box.h"
#include "util/u_region.h"
#include "util/u_vector.h"
#include "pipe/p_state.h"

void pipe_region_add(struct pipe_region *region, const struct pipe_box *in)
{
   struct pipe_box *cur;
   bool append = true;

   /* If we have too many small rectangles, punt to extents. */
   if (u_vector_length(&region->boxes) >= 64) {
      fprintf(stderr, "\t\tTOO MANY BOXES: dropping region %p to extents\n", region);
      u_vector_reset(&region->boxes);
      cur = u_vector_add(&region->boxes);
      memcpy(cur, &region->extent, sizeof(*cur));
   }

   /* See if we can just extend an existing box, e.g. texture atlas
    * with adjoining edges. */
   u_vector_foreach(cur, &region->boxes) {
      fprintf(stderr, "\t\tCONSIDERING target [%d, %d] + [%d, %d] to be enlarged by [%d, %d] + [%d, %d]\n", cur->x, cur->y, cur->width, cur->height, in->x, in->y, in->width, in->height);

      if (memcmp(cur, in, sizeof(*cur)) == 0)
         return;

      if (cur->x == in->x && cur->width == in->width) {
         if (cur->y + cur->height == in->y) {
            cur->height += in->height;
            append = false;
         }
         else if (cur->y == in->y + in->height) {
            cur->y = in->y;
            append = false;
         }
      }

      if (cur->y == in->y && cur->height == in->height) {
         if (cur->x + cur->width == in->x) {
            cur->width += in->width;
            append = false;
         }
         else if (cur->x == in->x + in->width) {
            cur->x = in->x;
            append = false;
         }
      }

      if (!append) {
         fprintf(stderr, "\textending box (%d, %d) + (%d, %d) to include (%d, %d) + (%d, %d)\n", cur->x, cur->y, cur->width, cur->height, in->x, in->y, in->width, in->height);
         break;
      }
   }

   /* No match; we need to append a new box. */
   if (append) {
      cur = u_vector_add(&region->boxes);
      memcpy(cur, in, sizeof(*cur));
      fprintf(stderr, "\tnew box (%d, %d) + (%d, %d)\n", cur->x, cur->y, cur->width, cur->height);
   }

   u_vector_foreach(cur, &region->boxes)
      fprintf(stderr, "\t\t\t[%d, %d] + [%d, %d]\n", cur->x, cur->y, cur->width, cur->height);

   /* Potentially enlarge the extent of the region. */
   u_box_union_2d(&region->extent, &region->extent, in);
   fprintf(stderr, "\tnew extent (%d, %d) + (%d, %d)\n", region->extent.x, region->extent.y, region->extent.width, region->extent.height);
}
